function groupBy (
  elements,
  cb
) {
  const groups = {};

  for (const each of elements) {
    const key = cb(each);

    const compiled = groups[key] ?? [];

    groups[key] = compiled;

    each.baseAmount = each.quantity * each.rate;

    each.gstAmount = ((each.quantity * each.rate) * 9) / 100;

    compiled.push(each);
  }

  return groups;
}

module.exports = groupBy;
