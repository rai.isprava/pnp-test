const billData = [
    {srNo:1,description:"sample description here",projectName:"sample Project Name",previousBill:1000,thisBill:5000,cumBill:15000},
    {srNo:2,description:"sample description here",projectName:"sample Project Name",previousBill:1000,thisBill:5000,cumBill:15000},
    {srNo:3,description:"sample description here",projectName:"sample Project Name",previousBill:1000,thisBill:5000,cumBill:15000},
    {srNo:4,description:"sample description here",projectName:"sample Project Name",previousBill:1000,thisBill:5000,cumBill:15000},
    {srNo:5,description:"sample description here",projectName:"sample Project Name",previousBill:1000,thisBill:5000,cumBill:15000}
];

const calculations = {

}

const instructions = [
    {id:1, text:"Kindly refer to previous payments, do not release payments beyond the percentage mentioned above."},
    {id:2, text:"Hold necessary payouts, that is security deposit and statuary deductions."},
    {id:3, text:"In case of any discrepancy and clarifications, please get in touch with the undersigned before proceeding for payments."}
]

module.exports = { billData, instructions};