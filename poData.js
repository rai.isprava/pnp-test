// const poData={
//     "purchaseOrderId": 8448,
//     "vendorName": "C.BHOGILAL WEST END PVT. LTD",
//     "vendorAddress": "B-1, HEM COLONY, S.V.ROAD, NEXT TO GOLDEN TOBACCO, OPP, IDBI BANK, VILE PARLE (W), MUMBAI - 400 056",
//     "vendorGstIn": "27AAACG2457Q1Z6",
//     "vendorPan": "AAACG2457Q",
//     "vendorAccName": "C.BHOGILAL WEST END PVT. LTD",
//     "vendorAccNumber": 920030021137497,
//     "vendorAccBranch": "Axis Bank, Mumbai",
//     "vendorAccIfsc": "UTIB0000064",
//     "shippingToName": "Regato Vaddo C",
//     "shippingToAddress": "P.T. Sheet No.9 Chalta No.72/15, Cunchelim, Mapusa, District North Goa, Pin code 403507",
//     "companyName": "Isprava Vesta Private Limited",
//     "companyAddress": "Aldeia Sereina, 542 9 B1 Boutawado\nAssagao Bardez, Goa - 403507",
//     "companyGstIn": "30AAACK0376Q1ZH",
//     "filename": "po ivpl c.bhogilal west end pvt. ltd regato vaddo c 2022-10-07 8448",
//     "redactedFilename": "po ivpl c.bhogilal west end pvt. ltd regato vaddo c 2022-10-07 8448_redacted",
//     "tallyReference": "PO_IVPL_8448",
//     "poDate": "07/10/2022",
//     "requisitioner": "Pooja Shah",
//     "shippedVia": "road",
//     "deliveryPoint": "delivery_to_project",
//     "paymentTerms": "15% advance and balance against delivery of material and after submitting the invoice",
//     "orders": [
//         {'orderId': 40276, 'productName': "SOUNTERTOP WASHBASIN |LOCATION: GUEST BATHROOM 01 |DESCRIPTION: FOSTER WASHBOWL 495mm | BRAND:DURAVIT |CAT NO: 033550 00 00", 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17851, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0401', 'unit': 'Nos', 'quantity': 1, 'rate': 3770, 'amount': 3770, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '9925f9fd93.jpg'
//     }, 
//     {'orderId': 40277, 'productName': 'UNDERCOUNTER WASHBASIN |LOCATION: GUEST BATHROOM 02 |DESCRIPTION:D-CODE VANITY BASIN UNDERCOUNTER WITH OVERFLOW 495 X 290 | BRAND:DURAVIT |CAT NO: 033849 00 17', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17852, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0401', 'unit': 'Nos', 'quantity': 1, 'rate': 2741, 'amount': 2741, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': 'ec17105db9.jpg'
// }, 
// {'orderId': 40282, 'productName': 'WASH BASIN FAUCET | LOCATION: , GUEST BATHROOM 02 |  BRAND: HANSGROHE | DESCRIPTION: HG VERNIS BASIC SET FOR SINGLE LEVER CONCEALED BATH MIXER | CAT NO: 71551007', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17853, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0402', 'unit': 'Nos', 'quantity': 1, 'rate': 4527, 'amount': 4527, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '58ad61bb43.jpg'
// }, 
// {'orderId': 40284, 'productName': 'WASH BASIN FAUCET | LOCATION: MASTER BATHROOM |  BRAND: HANSGROHE | DESCRIPTION:VERNIS SHAPE SINGLE LEVER BASIN MIXER 190 WITH POP-UP WASTE SET | CAT NO: 71562007', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17854, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0402', 'unit': 'Nos', 'quantity': 2, 'rate': 8113, 'amount': 16226, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': 'c17a98ff70.jpg'
// }, 
// {'orderId': 40283, 'productName': 'WASH BASIN FAUCET | LOCATION: POWDER ROOM | GUEST BATHROOM 01 BRAND: HANSGROHE | DESCRIPTION: VERNIS BLEND SINGLE LEVER BASIN MIXER 190 WITH POP-UP WASTE SET | CAT NO: 71552007', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17855, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0402', 'unit': 'Nos', 'quantity': 2, 'rate': 7315, 'amount': 14630, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '853a70fb15.jpg'
// }, 
// {'orderId': 40280, 'productName': 'W C | LOCATION: MASTER BATHROOM |  BRAND:DURAVIT | DESCRIPTION:DURASTYLE WC 370 x 540 | CAT NO: 255209 00 00', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17856, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0401', 'unit': 'Nos', 'quantity': 1, 'rate': 7949, 'amount': 7949, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '35c939b159.jpg'
// }, 
// {'orderId': 40278, 'productName': 'W C |LOCATION: POWDER ROOM,GUEST BATHROOM 01,GUEST BATHROOM 02 | BRAND:DURAVIT |DESCRIPTION:COMPACT DURASTYLE WC 370 X 480 |CAT NO: 253909 00 00', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17857, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0401', 'unit': 'Nos', 'quantity': 3, 'rate': 5259, 'amount': 15777, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '2359e04022.jpg'
// },
//  {'orderId': 40281, 'productName': 'W C SEAT COVER | LOCATION: MASTER BATHROOM |  BRAND:DURAVIT | DESCRIPTION:DURASTYLE SEAT & COVER | CAT NO: 006379 00 37', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17858, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0401', 'unit': 'Nos', 'quantity': 1, 'rate': 3646, 'amount': 3646, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '37b6ac8936.jpg'
// }, 
// {'orderId': 40279, 'productName': 'W C SEAT COVER | LOCATION: POWDER ROOM,GUEST BATHROOM 01,GUEST BATHROOM 02 | BRAND :DURAVIT | DESCRIPTION:DURASTYLE SEAT & COVER | CAT NO: 006379 00 37', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17859, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0401', 'unit': 'Nos', 'quantity': 3, 'rate': 3646, 'amount': 10938, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '2ee8f8b96a.jpg'
// }, 
// {'orderId': 40286, 'productName': 'ANGLE VALVE |  LOCATION: POWDERROOM,GUESTBATHROOM01,GUESTBATHROOM02,MASTERBATHROOM |  BRAND: HANSGROHE |  DESCRIPTION: HG ANGLE VALVE E DN15XDN15 1/2\" |  CAT NO: 13902000', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17860, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0402', 'unit': 'Nos', 'quantity': 4, 'rate': 500, 'amount': 2000, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': 'e01a83d251.jpg'
// }, 
// {'orderId': 40287, 'productName': 'BATH MIXER |  LOCATION:GUESTBATHROOM01,GUESTBATHROOM02 |  BRAND: HANSGROHE |  DESCRIPTION: | HG VERNIS BLEND FS BATH MIXER CHROME CN CAT NO: 71449007', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17861, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0402', 'unit': 'Nos', 'quantity': 2, 'rate': 1650, 'amount': 3300, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '29ff2dd0d1.jpg'
// }, {'orderId': 40288, 'productName': 'BATH MIXER |  LOCATION:MASTER BATHROOM |  BRAND: HANSGROHE |  DESCRIPTION: | HG VERNIS SHAPE FS BATH MIXER CHROME CN CAT NO: 71458007', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17862, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0402', 'unit': 'Nos', 'quantity': 1, 'rate': 2101, 'amount': 2101, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '189492e8cd.jpg'
// }, {'orderId': 40293, 'productName': 'BATH SPOUT |  LOCATION: GUEST BATHROOM 01,GUEST BATHROOM 02,MASTER BATHROOM | BRAND : HANSGROHE |  DESCRIPTION : BATH SPOUT WITH BUTTON ATTACHMENT |  CAT NO: 13423000', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17863, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0402', 'unit': 'Nos', 'quantity': 3, 'rate': 3851, 'amount': 11553, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': 'f5908c4f55.jpg'
// }, 
// {'orderId': 40294, 'productName': 'HAND SHOWER |  LOCATION: GUEST BATHROOM 01,GUEST BATHROOM 02, MASTER BATHROOM | BRAND : HANSGROHE |  DESCRIPTION : HAND SHOWER VARIO |  CAT NO: 26270007', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17864, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0402', 'unit': 'Nos', 'quantity': 3, 'rate': 1300, 'amount': 3900, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '49635b6175.jpg'
// }, 
// {'orderId': 40285, 'productName': 'HEALTH FAUCET SET |  LOCATION: POWDERROOM,GUESTBATHROOM01,GUESTBATHROOM02,MASTERBATHROOM |  BRAND: HANSGROHE |  DESCRIPTION: BIDETTE 1JET HAND SHOWER/ PORTER’S SHOWER HOLDER SET WITH PRESSURE SHOWER HOSE 1.25 M |  CAT NO: 32129000', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17865, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0402', 'unit': 'Nos', 'quantity': 4, 'rate': 2500, 'amount': 10000, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '98c5dc1d5a.jpg'
// }, 
// {'orderId': 40292, 'productName': 'SHOWER ARM |  LOCATION: GUEST BATHROOM 01,GUEST BATHROOM 02,MASTER BATHROOM | BRAND : HANSGROHE |  DESCRIPTION : HG STANDARD SHOWER ARM DN15 128MM CHROME |  CAT NO: 27809007', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17866, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0402', 'unit': 'Nos', 'quantity': 3, 'rate': 1930, 'amount': 5790, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '92ab87d95e.jpg'
// }, 
// {'orderId': 40290, 'productName': 'SHOWER HEAD |  LOCATION: GUEST BATHROOM 01,GUEST BATHROOM 02 | BRAND : HANSGROHE |  DESCRIPTION : HG CROMA E 100 VARIO SH CHROME |  CAT NO: 26271007', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17867, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0402', 'unit': 'Nos', 'quantity': 2, 'rate': 2760, 'amount': 5520, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '24c2b9e6f2.jpg'}
// , {'orderId': 40291, 'productName': 'SHOWER HEAD |  LOCATION: MASTER BATHROOM | BRAND : HANSGROHE |  DESCRIPTION : HG CROMA E 100 VARIO SH CHROME |  CAT NO: 26281007', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17868, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0402', 'unit': 'Nos', 'quantity': 1, 'rate': 2900, 'amount': 2900, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '3e41cb08e2.jpg'},
//  {'orderId': 40295, 'productName': 'SHOWER HOSE |  LOCATION: GUEST BATHROOM 01,GUEST BATHROOM 02,MASTER BATHROOM | BRAND : HANSGROHE |  DESCRIPTION : SHOWER HOSE 1250MM |  CAT NO: 28272000', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17869, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0402', 'unit': 'Nos', 'quantity': 3, 'rate': 850, 'amount': 2550, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '48ecb0fcf4.jpg'},
//   {'orderId': 40296, 'productName': 'HAND SHOWER HOLDER |  LOCATION: GUEST BATHROOM 01,GUEST BATHROOM 02,MASTER BATHROOM | BRAND : HANSGROHE |  DESCRIPTION : SHOWER HOLDER PORTER |  CAT NO: 27521000', 'description': '', 'onlineLink': '', 'productSku': "None", 'journeyId': 17870, 'externalDimensions': '0x0x0', 'internalDimensions': '0x0x0', 'deliveryDate': '2022-10-12', 'costCenter': 'mc0402', 'unit': 'Nos', 'quantity': 3, 'rate': 700, 'amount': 2100, 'transportationCost': 0, 'packagingCost': 0, 'cgstTax': 0, 'igstTax': 18, 'sgstTax': 0, 'thumbnail': '15ca49b05d.jpg'}]
//   };

//   const SHIPPED_VIA_MAP = new Map([
//     ["road", "Road"],
//     ["rail", "Rail"],
//     ["air", "Air"],
//     ["sea", "Sea"],
//   ]);

//   const DELIVERY_POINT_MAP = new Map([
//     ["ex_works", "Ex Works"],
//     ["delivery_to_project", "Delivery To Project"],
//     ["delivery_to_company", "Delivery To Company (Head Office)"],
//     ["delivery_to_warehouse", "Delivery To Warehouse"],
//   ]);


//   let {
//     reference,
//     tallyReference,
//     purchaseOrderId,
//     ispravaLogoUrl,
//     companyName,
//     companyAddress,
//     companyGstIn,
//     vendorName,
//     vendorAddress,
//     vendorGstIn,
//     vendorPan,
//     vendorAccNumber,
//     vendorAccIfsc,
//     vendorAccBranch,
//     vendorAccName,
//     shippingToName,
//     shippingToAddress,
//     poDate,
//     requisitioner,
//     shippedVia,
//     deliveryPoint,
//     paymentTerms,
//     orders,
//   } = poData;

//   const { subtotal, cgst, sgst, igst, transportation, packaging } = orders.reduce(
//     (compiled, each) => {
//       const { amount, cgst, sgst, igst, transportation, packaging } = each;

//       return {
//         subtotal: compiled.subtotal + amount,
//         cgst: compiled.cgst + (amount * cgst / 100),
//         sgst: compiled.sgst + (amount * sgst / 100),
//         igst: compiled.igst + (amount * igst / 100),
//         transportation: compiled.transportation + transportation,
//         packaging: compiled.packaging + packaging,
//       };
//     },
//     {
//       subtotal: 0,
//       cgst: 0,
//       sgst: 0,
//       igst: 0,
//       packaging: 0,
//       transportation: 0,
//     },
//   );

//   const total = subtotal + cgst + sgst + igst + packaging + transportation;

//   function sanitizeAddress (address){
//     if (Array.isArray(address)) {
//       const sanitized = address.map(each => each.trim());
  
//       return sanitized;
//     }
  
//     const sanitized = address.split("\n").map(each => each.trim());
  
//     return sanitized;
//   }

//   orders=orders.map((each)=>{
//     each.productName="SOUNTERTOP WASHBASIN|LOCATION:GUESTBATHROOM01|DESCRIPTION:FOSTERWASHBOWL495mm|BRAND:DURAVIT|CAT NO: 033550 00 00 my name is shivam rai who teh fuck you are";
//     return each;
//   }
//     )

//   const formattedPoData = {
//     purchaseOrderId: purchaseOrderId,
//     ispravaLogoUrl: ispravaLogoUrl,
//     reference: reference,
//     tallyReference: tallyReference,
//     companyName: companyName,
//     companyAddress: sanitizeAddress(companyAddress),
//     companyGstIn: companyGstIn,
//     vendorName: vendorName,
//     vendorAddress: sanitizeAddress(vendorAddress),
//     vendorGstIn: vendorGstIn,
//     vendorPan: vendorPan,
//     vendorAccNumber: vendorAccNumber,
//     vendorAccIfsc: vendorAccIfsc,
//     vendorAccBranch: vendorAccBranch,
//     vendorAccName: vendorAccName,
//     shippingToName: shippingToName,
//     shippingToAddress: sanitizeAddress(shippingToAddress),
//     correspondenceAddress: sanitizeAddress(companyAddress),
//     poDate: poDate,
//     requisitioner: requisitioner,
//     shippedVia: SHIPPED_VIA_MAP.get(shippedVia),
//     deliveryPoint: DELIVERY_POINT_MAP.get(deliveryPoint),
//     paymentTerms: paymentTerms,
//     orders: orders,
//     subtotal: subtotal,
//     cgst: cgst,
//     sgst: sgst,
//     igst: igst,
//     packaging: packaging,
//     transportation: transportation,
//     total: total,
//   };



  const purchaseOrderPdfArgs = {
    purchaseOrderId: 9061n,
    ispravaLogoUrl: "https://storage.googleapis.com/pnp-staging-9d151/isprava-logo.png?X-Goog-Algorithm=GOOG4-RSA-SHA256&X-Goog-Credential=pnp-gcs%40wise-aristotle.iam.gserviceaccount.com%2F20230207%2Fauto%2Fstorage%2Fgoog4_request&X-Goog-Date=20230207T052351Z&X-Goog-Expires=1200&X-Goog-SignedHeaders=host&X-Goog-Signature=5eaf0ac0485f79e7a7557688155f4a39a4546afad35ee5e899bfee3a256cd8d87d3eee66a87d377ca09e09c643be1dda718a7855ea5ce5e491d462f99d6c46eb458f99112a737f471d2dd1d5272d6cc59f1b2e3ed20604e03fc52a57bb37db24f213a7d43293250da00d3b38a7490eac655af157a4460ee329e637fe8226fbdccab2ae5a9880f1b0b7ad54655b801b134e1690d16be56d6ee758c8faf5241b10751acd6b4c6613f2e380d527b927d580b597ae04f538c936474b6e039e231eb80f80f5e3c2d87389fb6f2f7b595190f2fdb5b3a5dab19ececf6087af04e949c0f53d380391e4613ca96212a71c9499d0bc43dc6abab337b2aaf9dc6b81c9d50b",
    reference: "po_ivpl_cloudtail_india_private_limited_amazon_lodha_belmondo_12_2023_02_07_9061",
    tallyReference: "PO_IVPL_9061",
    companyName: "Isprava Vesta Private Limited",
    companyAddress: [
      "42A,1st Floor, Impression House",
      "G.D Ambekar Marg, Wadala West, Mumbai - 400031",
    ],
    companyGstIn: "27AAACK0376Q3Z2",
    vendorName: "Cloudtail India Private limited - Amazon",
    vendorEmail: "orders@isprava.com",
    vendorContact: "+91 7349314525",
    vendorAddress: [
      "Haryana",
    ],
    vendorGstIn: "06AAQCS4259Q1ZE",
    vendorPan: "AAQCS4259Q",
    vendorAccNumber: null,
    vendorAccIfsc: null,
    vendorAccBranch: null,
    vendorAccName: null,
    shippingToName: "Maharashtra IVPL",
    shippingToAddress: [
      "42A,1st Floor, Impression House",
      "G.D Ambekar Marg, Wadala West, Mumbai - 400031",
    ],
    correspondenceAddress: [
      "42A,1st Floor, Impression House",
      "G.D Ambekar Marg, Wadala West, Mumbai - 400031",
    ],
    poDate: "07/02/2023",
    requisitioner: "Mahesh Joshi",
    shippedVia: "Road",
    deliveryPoint: "Delivery To Warehouse",
    paymentTerms: "100% advance",
    orders: [
      {
        quantity: 1,
        unit: "Nos",
        name: "Deccan Decembemonth Transport Mumbai TransportProject:-Aurelia-DVendor:-ikeaPO Num:-1007110071MaterialType:-soft furnishingInvoice no:- Rs36900/-",
        description: "",
        dimensions: "ext: 0x0x0, int: 0x0x0",
        deliveryDate: "2023-02-07",
        costCenter: "hd03",
        rate: 3790,
        amount: 90087678,
        cgst: 0,
        sgst: 0,
        igst: 0,
        packaging: 0,
        transportation: 0,
        thumbnail: "",
        onlineLink: "",
        sku: null,
        journeyId: 21254,
        qrDataUrl: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMQAAADECAYAAADApo5rAAAAAklEQVR4AewaftIAAAjsSURBVO3BQY4kyZEAQdVA/f/Lug0eHHZyIJBZPUOuidgfrLX+42GtdTystY6HtdbxsNY6HtZax8Na63hYax0Pa63jYa11PKy1joe11vGw1joe1lrHw1rreFhrHT98SOVvqphU/qaKSWWqmFSmiknlmyomlZuKSeWNihuVv6niEw9rreNhrXU8rLWOH76s4ptU3qi4UZkqblQmlaliUvlExTdVTCpvVNyoTBU3Fd+k8k0Pa63jYa11PKy1jh9+mcobFf9mFZPKVDGp3FRMKlPFjcpUMal8QmWqmCo+ofJGxW96WGsdD2ut42GtdfzwX65iUrmpmFSmiqnipmJSmSpuVKaKSWWqeKNiUplUpooblanif8nDWut4WGsdD2ut44f/cRWTyjepTBU3KlPFpDJV3FRMKjcVk8qk8obKVPHf7GGtdTystY6Htdbxwy+r+E0qNypTxaQyqUwVb6jcVEwqU8Wk8kbFjcobFb+p4t/kYa11PKy1joe11vHDl6n8kyomlTcqJpWp4qZiUvmmiknlRmWqmFSmikllqphUpooblX+zh7XW8bDWOh7WWscPH6r4N1F5o+ITFb+p4qZiUpkqJpU3KiaVqeKm4r/Jw1rreFhrHQ9rreOHX6byRsWkMlXcVEwqk8pUMancqLxRcVPxhspU8YmKSWWqeEPljYoblTcqPvGw1joe1lrHw1rr+OFDKjcVb6hMFZPKVDGpTBU3KjcVk8pU8YbKVPFNFTcVk8qNyo3KVDGp3KhMFf+kh7XW8bDWOh7WWscPH6q4UbmpmComlRuVqeKNijcqJpWp4hMqv0llqphUpop/UsXf9LDWOh7WWsfDWuuwP/iAylRxo/JGxY3KVPGGyk3FpHJT8YbKb6qYVKaKT6j8popJZar4poe11vGw1joe1lrHDx+qmFSmik+o/JtUvKHyRsWNylTxRsWNylQxqdxUTCo3FZPKpDJV/KaHtdbxsNY6HtZah/3BB1TeqJhUbiomlaliUrmpuFGZKiaVNyo+ofJGxY3KGxVvqEwVNyo3FTcqU8UnHtZax8Na63hYax0/fKhiUvlExaQyVUwqn1CZKm4qJpUblTcqbio+UfFNKjcqU8VNxY3KVPFND2ut42GtdTystY4fvqxiUplU3qh4o2JSeUNlqnij4psqJpWbijdUpoo3KiaV31QxqUwVn3hYax0Pa63jYa112B98kcpNxRsqU8UnVKaKSWWqmFTeqPgmlTcqblRuKm5UbiomlaliUnmj4pse1lrHw1rreFhrHT/8ZSo3FVPFpPJGxW+qeENlqvibVKaKG5VPqEwVk8obFZPKVPGJh7XW8bDWOh7WWscPH1KZKiaVqWJSuVF5o+Km4qbipuJG5aZiUrmp+ITKjcpUcVMxqdxU/KaKb3pYax0Pa63jYa11/PBlKlPFJyomlaniRmWqmFRuKt6omFQmlZuKT1TcqLxR8U+quFGZKj7xsNY6HtZax8Na67A/+EUqU8UbKp+omFTeqJhUvqniEypvVLyhMlXcqEwVk8pNxaRyU/FND2ut42GtdTystQ77gw+o3FRMKlPFN6lMFTcqb1RMKlPFjcpNxaQyVUwqNxU3KlPFjcpNxaQyVUwq31TxiYe11vGw1joe1lrHD19WcVNxo/JGxRsqU8Wk8kbFpDJVTBWfUJkqblRuKiaVm4pJZVKZKiaVm4p/0sNa63hYax0Pa63jhw9VTCpvVNxU3KjcqEwVk8pNxY3KjcpUMan8TRWTylRxo/KGylQxqbyhclPxiYe11vGw1joe1lrHD/8yFZPKTcWkcqNyUzGp3FR8ouINlW+qmFSmipuKN1Smin+Th7XW8bDWOh7WWof9wQdUbipuVKaKT6jcVLyhMlXcqEwVk8pNxRsqNxU3KlPFpPJPqvibHtZax8Na63hYax0/fFnFpHJTcaMyVUwqNxV/U8VNxaTyiYo3VKaKm4oblZuKSWWq+ITKVPGJh7XW8bDWOh7WWscPf1nFGxWfULmpmFSmikllqphUvknlpmJSmSpuVKaKG5Wbik+oTBWTylTxTQ9rreNhrXU8rLUO+4O/SGWquFGZKt5QmSr+JpWbin+Syk3FpDJVTCpTxaQyVUwqNxW/6WGtdTystY6Htdbxw4dU3qiYVG4qblSmijdUpopvqphU3lCZKm5U3qiYVG4qJpWp4qZiUpkqJpUblaniEw9rreNhrXU8rLWOH76sYlL5hMpNxaQyVUwqNypTxTdVfJPKTcWkMqlMFZPKTcWNyk3FJyq+6WGtdTystY6Htdbxwy+rmFSmikllqphUJpVPVHyTyhsVNxW/qeKmYlKZVKaKm4pJZaqYKv6mh7XW8bDWOh7WWscP/zCVG5WbiknlEypvVHxCZar4RMUbKjcVb6jcqNyofKLiEw9rreNhrXU8rLUO+4P/YipTxaQyVUwqU8WkMlXcqLxR8YbKJypuVG4qJpWp4g2VqWJSmSomlaniEw9rreNhrXU8rLWOHz6k8jdVTBVvqNyoTBU3KjcVk8onKiaVm4pJZaq4qZhU3lCZKr6p4pse1lrHw1rreFhrHT98WcU3qbyhclNxo/KbKm5UpoqbikllUrlRuVH5RMUbKlPFjcpU8YmHtdbxsNY6HtZaxw+/TOWNijdUpopJZVL5hMpUMalMKlPFb6q4UZkqPqEyqXyiYlKZKn7Tw1rreFhrHQ9rreOH/2cqJpU3Km4qvkllqvhExaQyVUwqU8UbFZPKVDGp3Kj8poe11vGw1joe1lrHD/9jVN6omFQmlaliUpkqJpWp4hMqU8Wk8kbFGypvqNyoTBWTyk3FNz2stY6HtdbxsNY6fvhlFb+pYlKZKiaVqeKmYlKZKt5QmSqmiknlRmWq+KaKG5WbihuVSeWf9LDWOh7WWsfDWuuwP/iAyt9U8YbKVPGGyhsVb6jcVEwqU8WNyk3FpDJVvKEyVUwqNxX/pIe11vGw1joe1lqH/cFa6z8e1lrHw1rreFhrHQ9rreNhrXU8rLWOh7XW8bDWOh7WWsfDWut4WGsdD2ut42GtdTystY6Htdbxf8hn5neIKGt7AAAAAElFTkSuQmCC",
        qrName: "qr_21254_42761",
        totalGst: 15,
        totalGstAmount: 10986756,
        cancelledPoId: 123,
      },
      {
        quantity: 1,
        unit: "Nos",
        name: "Deccan Decembemonth Transport Mumbai TransportProject:-Aurelia-DVendor:-ikeaPO Num:-1007110071MaterialType:-soft furnishingInvoice no:- Rs36900/-",
        description: "",
        dimensions: "ext: 0x0x0, int: 0x0x0",
        deliveryDate: "2023-02-07",
        costCenter: "hd03",
        rate: 3790,
        amount: 90087678,
        cgst: 0,
        sgst: 0,
        igst: 0,
        packaging: 0,
        transportation: 0,
        thumbnail: "",
        onlineLink: "",
        sku: null,
        journeyId: 21254,
        qrDataUrl: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMQAAADECAYAAADApo5rAAAAAklEQVR4AewaftIAAAjsSURBVO3BQY4kyZEAQdVA/f/Lug0eHHZyIJBZPUOuidgfrLX+42GtdTystY6HtdbxsNY6HtZax8Na63hYax0Pa63jYa11PKy1joe11vGw1joe1lrHw1rreFhrHT98SOVvqphU/qaKSWWqmFSmiknlmyomlZuKSeWNihuVv6niEw9rreNhrXU8rLWOH76s4ptU3qi4UZkqblQmlaliUvlExTdVTCpvVNyoTBU3Fd+k8k0Pa63jYa11PKy1jh9+mcobFf9mFZPKVDGp3FRMKlPFjcpUMal8QmWqmCo+ofJGxW96WGsdD2ut42GtdfzwX65iUrmpmFSmiqnipmJSmSpuVKaKSWWqeKNiUplUpooblanif8nDWut4WGsdD2ut44f/cRWTyjepTBU3KlPFpDJV3FRMKjcVk8qk8obKVPHf7GGtdTystY6Htdbxwy+r+E0qNypTxaQyqUwVb6jcVEwqU8Wk8kbFjcobFb+p4t/kYa11PKy1joe11vHDl6n8kyomlTcqJpWp4qZiUvmmiknlRmWqmFSmikllqphUpooblX+zh7XW8bDWOh7WWscPH6r4N1F5o+ITFb+p4qZiUpkqJpU3KiaVqeKm4r/Jw1rreFhrHQ9rreOHX6byRsWkMlXcVEwqk8pUMancqLxRcVPxhspU8YmKSWWqeEPljYoblTcqPvGw1joe1lrHw1rr+OFDKjcVb6hMFZPKVDGpTBU3KjcVk8pU8YbKVPFNFTcVk8qNyo3KVDGp3KhMFf+kh7XW8bDWOh7WWscPH6q4UbmpmComlRuVqeKNijcqJpWp4hMqv0llqphUpop/UsXf9LDWOh7WWsfDWuuwP/iAylRxo/JGxY3KVPGGyk3FpHJT8YbKb6qYVKaKT6j8popJZar4poe11vGw1joe1lrHDx+qmFSmik+o/JtUvKHyRsWNylTxRsWNylQxqdxUTCo3FZPKpDJV/KaHtdbxsNY6HtZah/3BB1TeqJhUbiomlaliUrmpuFGZKiaVNyo+ofJGxY3KGxVvqEwVNyo3FTcqU8UnHtZax8Na63hYax0/fKhiUvlExaQyVUwqn1CZKm4qJpUblTcqbio+UfFNKjcqU8VNxY3KVPFND2ut42GtdTystY4fvqxiUplU3qh4o2JSeUNlqnij4psqJpWbijdUpoo3KiaV31QxqUwVn3hYax0Pa63jYa112B98kcpNxRsqU8UnVKaKSWWqmFTeqPgmlTcqblRuKm5UbiomlaliUnmj4pse1lrHw1rreFhrHT/8ZSo3FVPFpPJGxW+qeENlqvibVKaKG5VPqEwVk8obFZPKVPGJh7XW8bDWOh7WWscPH1KZKiaVqWJSuVF5o+Km4qbipuJG5aZiUrmp+ITKjcpUcVMxqdxU/KaKb3pYax0Pa63jYa11/PBlKlPFJyomlaniRmWqmFRuKt6omFQmlZuKT1TcqLxR8U+quFGZKj7xsNY6HtZax8Na67A/+EUqU8UbKp+omFTeqJhUvqniEypvVLyhMlXcqEwVk8pNxaRyU/FND2ut42GtdTystQ77gw+o3FRMKlPFN6lMFTcqb1RMKlPFjcpNxaQyVUwqNxU3KlPFjcpNxaQyVUwq31TxiYe11vGw1joe1lrHD19WcVNxo/JGxRsqU8Wk8kbFpDJVTBWfUJkqblRuKiaVm4pJZVKZKiaVm4p/0sNa63hYax0Pa63jhw9VTCpvVNxU3KjcqEwVk8pNxY3KjcpUMan8TRWTylRxo/KGylQxqbyhclPxiYe11vGw1joe1lrHD/8yFZPKTcWkcqNyUzGp3FR8ouINlW+qmFSmipuKN1Smin+Th7XW8bDWOh7WWof9wQdUbipuVKaKT6jcVLyhMlXcqEwVk8pNxRsqNxU3KlPFpPJPqvibHtZax8Na63hYax0/fFnFpHJTcaMyVUwqNxV/U8VNxaTyiYo3VKaKm4oblZuKSWWq+ITKVPGJh7XW8bDWOh7WWscPf1nFGxWfULmpmFSmikllqphUvknlpmJSmSpuVKaKG5Wbik+oTBWTylTxTQ9rreNhrXU8rLUO+4O/SGWquFGZKt5QmSr+JpWbin+Syk3FpDJVTCpTxaQyVUwqNxW/6WGtdTystY6Htdbxw4dU3qiYVG4qblSmijdUpopvqphU3lCZKm5U3qiYVG4qJpWp4qZiUpkqJpUblaniEw9rreNhrXU8rLWOH76sYlL5hMpNxaQyVUwqNypTxTdVfJPKTcWkMqlMFZPKTcWNyk3FJyq+6WGtdTystY6Htdbxwy+rmFSmikllqphUJpVPVHyTyhsVNxW/qeKmYlKZVKaKm4pJZaqYKv6mh7XW8bDWOh7WWscP/zCVG5WbiknlEypvVHxCZar4RMUbKjcVb6jcqNyofKLiEw9rreNhrXU8rLUO+4P/YipTxaQyVUwqU8WkMlXcqLxR8YbKJypuVG4qJpWp4g2VqWJSmSomlaniEw9rreNhrXU8rLWOHz6k8jdVTBVvqNyoTBU3KjcVk8onKiaVm4pJZaq4qZhU3lCZKr6p4pse1lrHw1rreFhrHT98WcU3qbyhclNxo/KbKm5UpoqbikllUrlRuVH5RMUbKlPFjcpU8YmHtdbxsNY6HtZaxw+/TOWNijdUpopJZVL5hMpUMalMKlPFb6q4UZkqPqEyqXyiYlKZKn7Tw1rreFhrHQ9rreOH/2cqJpU3Km4qvkllqvhExaQyVUwqU8UbFZPKVDGp3Kj8poe11vGw1joe1lrHD/9jVN6omFQmlaliUpkqJpWp4hMqU8Wk8kbFGypvqNyoTBWTyk3FNz2stY6HtdbxsNY6fvhlFb+pYlKZKiaVqeKmYlKZKt5QmSqmiknlRmWq+KaKG5WbihuVSeWf9LDWOh7WWsfDWuuwP/iAyt9U8YbKVPGGyhsVb6jcVEwqU8WNyk3FpDJVvKEyVUwqNxX/pIe11vGw1joe1lqH/cFa6z8e1lrHw1rreFhrHQ9rreNhrXU8rLWOh7XW8bDWOh7WWsfDWut4WGsdD2ut42GtdTystY6Htdbxf8hn5neIKGt7AAAAAElFTkSuQmCC",
        qrName: "qr_21254_42761",
        totalGst: 15,
        totalGstAmount: 10986756,
        cancelledPoId: null,
      },
      {
        quantity: 1,
        unit: "Nos",
        name: "Deccan Decembemonth Transport Mumbai TransportProject:-Aurelia-DVendor:-ikeaPO Num:-1007110071MaterialType:-soft furnishingInvoice no:- Rs36900/-",
        description: "",
        dimensions: "ext: 0x0x0, int: 0x0x0",
        deliveryDate: "2023-02-07",
        costCenter: "hd03",
        rate: 3790,
        amount: 90087678,
        cgst: 0,
        sgst: 0,
        igst: 0,
        packaging: 0,
        transportation: 0,
        thumbnail: "",
        onlineLink: "",
        sku: null,
        journeyId: 21254,
        qrDataUrl: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMQAAADECAYAAADApo5rAAAAAklEQVR4AewaftIAAAjsSURBVO3BQY4kyZEAQdVA/f/Lug0eHHZyIJBZPUOuidgfrLX+42GtdTystY6HtdbxsNY6HtZax8Na63hYax0Pa63jYa11PKy1joe11vGw1joe1lrHw1rreFhrHT98SOVvqphU/qaKSWWqmFSmiknlmyomlZuKSeWNihuVv6niEw9rreNhrXU8rLWOH76s4ptU3qi4UZkqblQmlaliUvlExTdVTCpvVNyoTBU3Fd+k8k0Pa63jYa11PKy1jh9+mcobFf9mFZPKVDGp3FRMKlPFjcpUMal8QmWqmCo+ofJGxW96WGsdD2ut42GtdfzwX65iUrmpmFSmiqnipmJSmSpuVKaKSWWqeKNiUplUpooblanif8nDWut4WGsdD2ut44f/cRWTyjepTBU3KlPFpDJV3FRMKjcVk8qk8obKVPHf7GGtdTystY6Htdbxwy+r+E0qNypTxaQyqUwVb6jcVEwqU8Wk8kbFjcobFb+p4t/kYa11PKy1joe11vHDl6n8kyomlTcqJpWp4qZiUvmmiknlRmWqmFSmikllqphUpooblX+zh7XW8bDWOh7WWscPH6r4N1F5o+ITFb+p4qZiUpkqJpU3KiaVqeKm4r/Jw1rreFhrHQ9rreOHX6byRsWkMlXcVEwqk8pUMancqLxRcVPxhspU8YmKSWWqeEPljYoblTcqPvGw1joe1lrHw1rr+OFDKjcVb6hMFZPKVDGpTBU3KjcVk8pU8YbKVPFNFTcVk8qNyo3KVDGp3KhMFf+kh7XW8bDWOh7WWscPH6q4UbmpmComlRuVqeKNijcqJpWp4hMqv0llqphUpop/UsXf9LDWOh7WWsfDWuuwP/iAylRxo/JGxY3KVPGGyk3FpHJT8YbKb6qYVKaKT6j8popJZar4poe11vGw1joe1lrHDx+qmFSmik+o/JtUvKHyRsWNylTxRsWNylQxqdxUTCo3FZPKpDJV/KaHtdbxsNY6HtZah/3BB1TeqJhUbiomlaliUrmpuFGZKiaVNyo+ofJGxY3KGxVvqEwVNyo3FTcqU8UnHtZax8Na63hYax0/fKhiUvlExaQyVUwqn1CZKm4qJpUblTcqbio+UfFNKjcqU8VNxY3KVPFND2ut42GtdTystY4fvqxiUplU3qh4o2JSeUNlqnij4psqJpWbijdUpoo3KiaV31QxqUwVn3hYax0Pa63jYa112B98kcpNxRsqU8UnVKaKSWWqmFTeqPgmlTcqblRuKm5UbiomlaliUnmj4pse1lrHw1rreFhrHT/8ZSo3FVPFpPJGxW+qeENlqvibVKaKG5VPqEwVk8obFZPKVPGJh7XW8bDWOh7WWscPH1KZKiaVqWJSuVF5o+Km4qbipuJG5aZiUrmp+ITKjcpUcVMxqdxU/KaKb3pYax0Pa63jYa11/PBlKlPFJyomlaniRmWqmFRuKt6omFQmlZuKT1TcqLxR8U+quFGZKj7xsNY6HtZax8Na67A/+EUqU8UbKp+omFTeqJhUvqniEypvVLyhMlXcqEwVk8pNxaRyU/FND2ut42GtdTystQ77gw+o3FRMKlPFN6lMFTcqb1RMKlPFjcpNxaQyVUwqNxU3KlPFjcpNxaQyVUwq31TxiYe11vGw1joe1lrHD19WcVNxo/JGxRsqU8Wk8kbFpDJVTBWfUJkqblRuKiaVm4pJZVKZKiaVm4p/0sNa63hYax0Pa63jhw9VTCpvVNxU3KjcqEwVk8pNxY3KjcpUMan8TRWTylRxo/KGylQxqbyhclPxiYe11vGw1joe1lrHD/8yFZPKTcWkcqNyUzGp3FR8ouINlW+qmFSmipuKN1Smin+Th7XW8bDWOh7WWof9wQdUbipuVKaKT6jcVLyhMlXcqEwVk8pNxRsqNxU3KlPFpPJPqvibHtZax8Na63hYax0/fFnFpHJTcaMyVUwqNxV/U8VNxaTyiYo3VKaKm4oblZuKSWWq+ITKVPGJh7XW8bDWOh7WWscPf1nFGxWfULmpmFSmikllqphUvknlpmJSmSpuVKaKG5Wbik+oTBWTylTxTQ9rreNhrXU8rLUO+4O/SGWquFGZKt5QmSr+JpWbin+Syk3FpDJVTCpTxaQyVUwqNxW/6WGtdTystY6Htdbxw4dU3qiYVG4qblSmijdUpopvqphU3lCZKm5U3qiYVG4qJpWp4qZiUpkqJpUblaniEw9rreNhrXU8rLWOH76sYlL5hMpNxaQyVUwqNypTxTdVfJPKTcWkMqlMFZPKTcWNyk3FJyq+6WGtdTystY6Htdbxwy+rmFSmikllqphUJpVPVHyTyhsVNxW/qeKmYlKZVKaKm4pJZaqYKv6mh7XW8bDWOh7WWscP/zCVG5WbiknlEypvVHxCZar4RMUbKjcVb6jcqNyofKLiEw9rreNhrXU8rLUO+4P/YipTxaQyVUwqU8WkMlXcqLxR8YbKJypuVG4qJpWp4g2VqWJSmSomlaniEw9rreNhrXU8rLWOHz6k8jdVTBVvqNyoTBU3KjcVk8onKiaVm4pJZaq4qZhU3lCZKr6p4pse1lrHw1rreFhrHT98WcU3qbyhclNxo/KbKm5UpoqbikllUrlRuVH5RMUbKlPFjcpU8YmHtdbxsNY6HtZaxw+/TOWNijdUpopJZVL5hMpUMalMKlPFb6q4UZkqPqEyqXyiYlKZKn7Tw1rreFhrHQ9rreOH/2cqJpU3Km4qvkllqvhExaQyVUwqU8UbFZPKVDGp3Kj8poe11vGw1joe1lrHD/9jVN6omFQmlaliUpkqJpWp4hMqU8Wk8kbFGypvqNyoTBWTyk3FNz2stY6HtdbxsNY6fvhlFb+pYlKZKiaVqeKmYlKZKt5QmSqmiknlRmWq+KaKG5WbihuVSeWf9LDWOh7WWsfDWuuwP/iAyt9U8YbKVPGGyhsVb6jcVEwqU8WNyk3FpDJVvKEyVUwqNxX/pIe11vGw1joe1lqH/cFa6z8e1lrHw1rreFhrHQ9rreNhrXU8rLWOh7XW8bDWOh7WWsfDWut4WGsdD2ut42GtdTystY6Htdbxf8hn5neIKGt7AAAAAElFTkSuQmCC",
        qrName: "qr_21254_42761",
        totalGst: 15,
        totalGstAmount: 10986756,
        cancelledPoId: "",
      },
    ],
    subtotal: 379,
    cgst: 0,
    sgst: 0,
    igst: 0,
    packaging: 0,
    transportation: 0,
    total: 379,
  };

  module.exports= purchaseOrderPdfArgs;

