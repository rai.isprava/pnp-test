var express = require('express');
var app = express();
const poData = require("./poData");
const { billData, instructions } = require("./sampleBillData");
const boqdata =  require("./boqData");

// set the view engine to ejs
app.set('view engine', 'ejs');

app.use( express.static( "public" ) );

// use res.render to load up an ejs view file

// index page
// app.get('/', function(req, res) {
//   res.render('pages/index',{billData: billData, instructions: instructions});
// });


// app.get('/', function(req,res) {
//   res.render("pages/po", {...poData})
// })
//boq page
app.get('/', function(req,res) {
  res.render('pages/boq', {...boqdata})
})

// about page
app.get('/about', function(req, res) {
  res.render('pages/about');
});

app.listen(8080);
console.log('Server is listening on port 8080');

// auto/60px 70px 60px 1fr 100px 120px;

